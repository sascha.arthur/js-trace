import { expect } from 'chai';

import {environment} from '../environment/environment';
import {Report} from '../util/report';
import {StaticLogger} from '../util/static-logger';

describe('Report Basic Tests', () => {
    it('should create an android report', () => {
        environment.silentMode=true;
        environment.appType='android';

        const result = StaticLogger.getInstance();
        const report: Report = result.logError(new Error('test'));

        expect(report.version).to.eq(environment.versionString);
        expect(report.versionInt).to.eq(environment.versionInt);
        expect(report.appType).to.eq('android');
        expect(report.message).to.eq('test');
        expect(report.stack).to.not.eq(null);
        expect(report.stack).to.not.eq(undefined);
    });

    it('should create an ios report', () => {
        environment.silentMode=true;
        environment.appType='ios';

        const result = StaticLogger.getInstance();
        const report: Report = result.logError(new Error('test'));

        expect(report.version).to.eq(environment.versionString);
        expect(report.versionInt).to.eq(environment.versionInt);
        expect(report.appType).to.eq('ios');
        expect(report.message).to.eq('test');
        expect(report.stack).to.not.eq(null);
        expect(report.stack).to.not.eq(undefined);
    });
});
