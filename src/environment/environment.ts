export const environment = {
    production: false,
    versionString: '0.0.0',
    versionInt: 1,
    appType: 'unknown', // unknown | android | ios | web
    silentMode: false, // prevents all output on console, should be enabled on production
    receivingEndpoint: 'https://example.com/service/bugreport', // https endpoint for posting reports against
};
