import * as StackTrace from 'stacktrace-js';

import {version} from '../../package.json';
import {environment} from '../environment/environment';
import {LogLevel} from './log-level';
import {Report} from './report';

/**
 * StaticLogger
 */
export class StaticLogger {
    private static instance: StaticLogger = new StaticLogger();
    errorCount = 0;
    expectedErrorCount = 0;
    unknownHandlingCount = 0;

    static getInstance(): StaticLogger {
        return StaticLogger.instance;
    }

    /**
     * This function can be called from anywhere typescript is running.
     * This function is typesafe for the 'Error' object
     *
     * @param e
     * @param logLevel
     */
    logError(e: Error, logLevel?: LogLevel): Report { // this can be called from everywhere
        const rep: Report = new Report();
        rep.message = e.message;
        rep.logLevel = logLevel;

        return this.report(rep);
    }

    /**
     * This function can be called from anywhere typescript is running.
     * For the beginning we try to parse errors, which is not typesafe, long term this should be made typesafe
     *
     * @param e: any
     * @param logLevel
     * @param additional?
     * @param additionalStack? StackTrace.StackFrame[] addtional stack (for example, from calling thread)
     */
    logTypeUnsafeException(e: any, logLevel?: LogLevel, additional?: string, additionalStack?: StackTrace.StackFrame[]): Report { // this can be called from everywhere
        const rep: Report = new Report();
        const errorType: string = typeof e;

        const stack: StackTrace.StackFrame[] = StaticLogger.getInstance().getCurrentStack();

        if (additionalStack !== undefined) {
            additionalStack.forEach(frame => {
                stack.push(frame);
            });
        }

        rep.additional = additional ? additional : null;
        rep.stack = stack;
        rep.logLevel = logLevel;

        if (errorType === 'object') {
            switch (e.constructor.name) {
                case 'SyntaxError':

                    try {
                        const tsError: SyntaxError = e;
                        rep.message = tsError.message;
                    } catch (e) {
                        this.logTypeUnsafeException(e, LogLevel.SEVERE,'logTypeUnsafeException::SyntaxError', stack); // recursion here, but different error!
                    }
                    break;
                case 'TypeError':

                    try {
                        const tsError: TypeError = e;
                        rep.message = tsError.message;
                    } catch (e) {
                        this.logTypeUnsafeException(e, LogLevel.SEVERE,'logTypeUnsafeException::TypeError', stack); // recursion here, but different error!
                    }
                    break;
                case 'Error':

                    try {
                        const tsError: Error = e;
                        rep.message = tsError.message;
                    } catch (e) {
                        this.logTypeUnsafeException(e, LogLevel.SEVERE,'logTypeUnsafeException::Error', stack); // recursion here, but different error!
                    }
                    break;
                default:
                    rep.message = 'object-type not known:' + e.constructor.name + ' error: ' + JSON.stringify(e);
                    this.unknownHandlingCount++;
            }
        } else {
            rep.message = 'error not object: ' + errorType + ' error: ' + JSON.stringify(e);
        }

        return this.report(rep);
    }

    /**
     * This is the final reporting function where the Report goes through
     *
     * @param report
     */
    report(report: Report): Report {
        this.errorCount++;

        if (report.stack == null) {
            report.stack = this.getCurrentStack();
        }

        report.version = environment.versionString;
        report.versionInt = environment.versionInt;
        report.appType = environment.appType;
        report.tsTraceVersion = version;

        // send only on production..
        if (!environment.production) {
            if(!environment.silentMode) {
                console.log('>>>>>>>>>> **************************************************');
                console.log('----- Report json -----');
                console.log(JSON.stringify(report));
                console.log('----- Report.message -----');
                console.log(report.message);
                console.log('----- Report.additional -----');
                console.log(report.additional);
                console.log('----- Report.stack -----');
                console.log(report.stack);
                console.log('<<<<<<<<<< **************************************************');
            }
            return report;
        }

        this.uploadReport(report);

        return report;
    }

    /**
     * This function is finally uploading the Report to our servers.
     * TODO: currently this is unsafe, since it only uploads, when the device is online, it should be made failsafe
     *
     * @param report
     */
    private uploadReport(report: Report): void {

        setTimeout(() => {

            const url = environment.receivingEndpoint + `?type=${report.appType}&version=${report.versionInt}`;

            try {
                const http = new XMLHttpRequest();
                http.open('POST', url, true);

                http.setRequestHeader('Content-type', 'text/plain;charset=UTF-8');

                http.onreadystatechange = () => {
                    if (http.readyState === 4 && http.status === 200) {
                        if(!environment.silentMode) {
                            console.log('report send', report);
                        }
                    }
                };
                http.send(JSON.stringify(report));

            } catch (e) {
                if(!environment.silentMode) {
                    console.error('failed to upload report..', e);
                }
            }
        }, 50);
    }

    /**
     * Get current stack
     *
     * @return StackTrace.StackFrame[]
     */
    getCurrentStack(): StackTrace.StackFrame[] {

        let stack: StackTrace.StackFrame[] = [];
        try {
            stack = StackTrace.getSync();
        } catch (e) {
            if(!environment.silentMode) {
                console.error('something bad happened getting current stack..', e);
            }
        }

        return stack;
    }
}
