import * as StackTrace from 'stacktrace-js';

import { LogLevel } from '@src/util/log-level';

/**
 * Report
 *
 */
export class Report {

    /**
     * message & stack
     */
    message: string = null;
    stack: StackTrace.StackFrame[] = null;

    /**
     * additional
     */
    additional: string = null;

    /**
     * meta infos
     */
    appType: string;
    logLevel: LogLevel = LogLevel.SEVERE;
    version: string;
    versionInt: number;

    tsTraceVersion: string;
}
