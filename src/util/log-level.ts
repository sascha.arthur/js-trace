/**
 * LogLevel
 *
 */
export const enum LogLevel {
    SEVERE = 1000, // default as most likely unknown
    WARNING = 900, // known bugs, blocked by ns
    INFO = 800, // info logs (e.g. server errors like timeouts)
    CONFIG = 700,
    FINE = 500,
    FINER = 400,
    FINEST = 300
}
